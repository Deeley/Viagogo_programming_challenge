from random import randint, random
import time

# multi dimensional array that stores [ x, y, price, tickets_left, distance, event number]
rows = 10
cols = 6

events = [[0] * cols for i in range(rows)]
i = 0
print("Generating 10 random events...")
while i < 10:
    x = randint(-10, 10)
    y = randint(-10, 10)
    price = randint(0, 20) + randint(0, 80) / 100
    tickets_left = randint(0, 300)

    events[i][0] = x
    events[i][1] = y
    events[i][2] = price
    events[i][3] = tickets_left
    events[i][4] = 0
    events[i][5] = i

    print("Event: ", i, ": ", events[i])

    i = i + 1

# gets amount of events
num_of_events = len(events)
print(num_of_events)

while True:
    # get user input
    user_input = input("Please input Coordinates: ")

    # split into x and y co-ords
    num1, num2 = user_input.split(',', 1)

    # converts string to int
    x_coord = int(num1)
    y_coord = int(num2)

    # ensures numbers are in correct range
    if (-10 <= x_coord <= 10) and (-10 <= y_coord <= 10):
        break
    else:
        print("Invalid co-ordinates")

print("Closest events to: ", x_coord, ",", y_coord)
i = 0
while i < 10:

    new_x = (x_coord - events[i][0])
    new_y = (y_coord - events[i][1])

    # calculates manhattan distance from user coordinates
    events[i][4] = (new_x + new_y)

    # converts potentially negative number to positive
    if events[i][4] < 0:
        events[i][4] = events[i][4] * -1

    i = i + 1

i = 0
# sorts array using distance value as key
events.sort(key=lambda x:x[4])

# final output displays closest events and cost
while i < 5 and events[i][3] > 0:
    print("Event ", events[i][5], " - £", events[i][2], ", Distance ", events[i][4])
    i = i + 1

time.sleep(10)
